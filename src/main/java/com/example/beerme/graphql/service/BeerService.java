package com.example.beerme.graphql.service;

import java.util.List;
import java.util.Optional;

import com.example.beerme.graphql.domain.Beer;

/**
 * Service Interface for managing {@link Beer}.
 * 
 * 
 *
 */
public interface BeerService {

	/**
	 * Save a beer.
	 *
	 * @param beer the entity to save.
	 * @return the persisted entity.
	 */
	Beer save(Beer beer);

	/**
	 * Get all the beers.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	List<Beer> findAll();

	/**
	 * Get the "id" beer.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<Beer> findOne(Long id);

	/**
	 * Delete the "id" beer.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);

}
