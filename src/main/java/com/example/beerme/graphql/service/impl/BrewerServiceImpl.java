package com.example.beerme.graphql.service.impl;

import java.util.Optional;

import com.example.beerme.graphql.domain.Brewer;
import com.example.beerme.graphql.repository.BrewerRepository;
import com.example.beerme.graphql.service.BrewerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Brewer}.
 * 
 * 
 *
 */
@Service
@Transactional
public class BrewerServiceImpl implements BrewerService {

	private final Logger log = LoggerFactory.getLogger(BrewerServiceImpl.class);

	private final BrewerRepository brewerRepository;

	public BrewerServiceImpl(BrewerRepository brewerRepository) {
		this.brewerRepository = brewerRepository;
	}

	/**
	 * Save a brewer.
	 *
	 * @param brewer the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Brewer save(Brewer brewer) {
		log.debug("Request to save Brewer : {}", brewer);
		return brewerRepository.save(brewer);
	}

	/**
	 * Get all the brewers.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Brewer> findAll(Pageable pageable) {
		log.debug("Request to get all Brewers");
		return brewerRepository.findAll(pageable);
	}

	/**
	 * Get one brewer by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Brewer> findOne(Long id) {
		log.debug("Request to get Brewer : {}", id);
		return brewerRepository.findById(id);
	}

	/**
	 * Delete the brewer by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Brewer : {}", id);
		brewerRepository.deleteById(id);
	}
}
