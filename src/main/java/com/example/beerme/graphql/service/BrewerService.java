package com.example.beerme.graphql.service;

import java.util.Optional;

import com.example.beerme.graphql.domain.Brewer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Brewer}.
 *
 */
public interface BrewerService {

	/**
	 * Save a brewer.
	 *
	 * @param brewer the entity to save.
	 * @return the persisted entity.
	 */
	Brewer save(Brewer brewer);

	/**
	 * Get all the brewers.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	Page<Brewer> findAll(Pageable pageable);

	/**
	 * Get the "id" brewer.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	Optional<Brewer> findOne(Long id);

	/**
	 * Delete the "id" brewer.
	 *
	 * @param id the id of the entity.
	 */
	void delete(Long id);
}
