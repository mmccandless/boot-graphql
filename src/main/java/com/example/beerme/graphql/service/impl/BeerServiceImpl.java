package com.example.beerme.graphql.service.impl;

import java.util.List;
import java.util.Optional;

import com.example.beerme.graphql.domain.Beer;
import com.example.beerme.graphql.repository.BeerRepository;
import com.example.beerme.graphql.service.BeerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Beer}.
 * 
 * 
 *
 */
@Service
@Transactional
public class BeerServiceImpl implements BeerService {

	private final Logger log = LoggerFactory.getLogger(BeerServiceImpl.class);

	private final BeerRepository beerRepository;

	public BeerServiceImpl(BeerRepository beerRepository) {
		this.beerRepository = beerRepository;
	}

	/**
	 * Save a beer.
	 *
	 * @param beer the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Beer save(Beer beer) {
		log.debug("Request to save Beer : {}", beer);
		return beerRepository.save(beer);
	}

	/**
	 * Get all the beers.
	 *
	 * @param pageable the pagination information.
	 * @return the list of entities.
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Beer> findAll() {
		log.debug("Request to get all beers");
		return beerRepository.findAll();
	}

	/**
	 * Get one beer by id.
	 *
	 * @param id the id of the entity.
	 * @return the entity.
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Beer> findOne(Long id) {
		log.debug("Request to get Beer : {}", id);
		return beerRepository.findById(id);
	}

	/**
	 * Delete the beer by id.
	 *
	 * @param id the id of the entity.
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Beer : {}", id);
		beerRepository.deleteById(id);
	}

}
