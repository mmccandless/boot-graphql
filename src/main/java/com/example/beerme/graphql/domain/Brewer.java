package com.example.beerme.graphql.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A very simple Brewer entity
 * 
 * 
 *
 */
@Entity
@Table(name = "brewer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Brewer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	// @TODO GraphQL can not lazily load relational data !!!
	@OneToMany(mappedBy = "brewer", fetch = FetchType.EAGER)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@JsonManagedReference(value="auth-beers")
	private Set<Beer> beers = new HashSet<>();
	/**
	 * No-arg constructor
	 */
	public Brewer() {
		super();
	}

	/**
	 * Constructor that takes name of it
	 * 
	 * @param name
	 */
	public Brewer(@NotNull String name) {
		super();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Brewer name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Beer> getbeers() {
		return beers;
	}

	public Brewer beers(Set<Beer> beers) {
		this.beers = beers;
		return this;
	}

	public Brewer addbeers(Beer beer) {
		this.beers.add(beer);
		beer.setBrewer(this);
		return this;
	}

	public Brewer removebeers(Beer beer) {
		this.beers.remove(beer);
		beer.setBrewer(null);
		return this;
	}

	public void setbeers(Set<Beer> beers) {
		this.beers = beers;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Brewer)) {
			return false;
		}
		return id != null && id.equals(((Brewer) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Brewer{" + "id=" + getId() + ", name='" + getName() + "'" + "}";
	}
}