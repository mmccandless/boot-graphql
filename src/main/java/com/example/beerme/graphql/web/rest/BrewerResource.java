package com.example.beerme.graphql.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.example.beerme.graphql.domain.Brewer;
import com.example.beerme.graphql.service.BrewerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing
 * {@link com.example.beerme.graphql.domain.Brewer}.
 * 
 * 
 *
 */
@RestController
@RequestMapping("/api")
public class BrewerResource {

	private final Logger log = LoggerFactory.getLogger(BrewerResource.class);

	private final BrewerService brewerService;

	public BrewerResource(BrewerService brewerService) {
		this.brewerService = brewerService;
	}

	/**
	 * {@code POST  /brewers} : Create a new brewer.
	 *
	 * @param brewer the brewer to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new brewer, or with status {@code 417 (Expectation Failed)}
	 *         if the brewer has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/brewers")
	public ResponseEntity<Brewer> createBrewer(@Valid @RequestBody Brewer brewer) throws URISyntaxException {
		log.debug("REST request to save Brewer : {}", brewer);
		if (brewer.getId() != null) {
			throw new URISyntaxException("A new Brewer must not have an ID", "PRIMARY KEY will be set by application");
		}

		try {
			Brewer result = brewerService.save(brewer);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * {@code PUT  /brewers} : Updates an existing brewer.
	 *
	 * @param brewer the brewer to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated brewer, or with status {@code 417 (Expectation Failed)}
	 *         if the brewer is not valid.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/brewers")
	public ResponseEntity<Brewer> updateBrewer(@Valid @RequestBody Brewer brewer) throws URISyntaxException {
		log.debug("REST request to update Brewer : {}", brewer);
		if (brewer.getId() == null) {
			throw new ApiException("Brewer must have an ID to update", "PRIMARY KEY is required to udpate an entity");
		}

		try {
			Brewer result = brewerService.save(brewer);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * {@code GET  /brewers} : get all the brewers.
	 *
	 * @param pageable the pagination information.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of brewers in body.
	 */
	@GetMapping("/brewers")
	public ResponseEntity<List<Brewer>> getAllBrewers(Pageable pageable) {
		log.debug("REST request to get a page of Brewers");
		Page<Brewer> page = brewerService.findAll(pageable);
		return ResponseEntity.ok().body(page.getContent());
	}

	/**
	 * {@code GET  /brewers/:id} : get the "id" brewer.
	 *
	 * @param id the id of the brewer to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the brewer, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/brewers/{id}")
	public ResponseEntity<Brewer> getBrewer(@PathVariable Long id) {
		log.debug("REST request to get Brewer : {}", id);
		Optional<Brewer> brewer = brewerService.findOne(id);

		if (brewer.isPresent()) {
			return new ResponseEntity<>(brewer.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	/**
	 * {@code DELETE  /brewers/:id} : delete the "id" brewer.
	 *
	 * @param id the id of the brewer to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}, or
	 *         with status {@code 417 (Expectation Failed)} if the operation failed.
	 */
	@DeleteMapping("/brewers/{id}")
	public ResponseEntity<Void> deleteBrewer(@PathVariable Long id) {
		log.debug("REST request to delete Brewer : {}", id);

		try {
			brewerService.delete(id);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.EXPECTATION_FAILED);
		}
	}
}
