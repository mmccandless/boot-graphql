package com.example.beerme.graphql.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.example.beerme.graphql.domain.Beer;
import com.example.beerme.graphql.service.BeerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing
 * {@link com.example.beerme.graphql.domain.Beer}.
 * 
 * 
 *
 */
@RestController
@RequestMapping("/api")
public class BeerResource {

	private final Logger log = LoggerFactory.getLogger(BeerResource.class);

	private final BeerService beerService;

	public BeerResource(BeerService beerService) {
		this.beerService = beerService;
	}

	/**
	 * {@code POST  /beers} : Create a new beer.
	 *
	 * @param beer the beer to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new beer, or with status {@code 417 (Expectation Failed)} if
	 *         the beer has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/beers")
	public ResponseEntity<Beer> createBeer(@Valid @RequestBody Beer beer) throws URISyntaxException {
		log.debug("REST request to save Beer : {}", beer);

		try {
			if (beer.getId() != null) {
				throw new ApiException("A new beer cannot already have an ID id exists",
						"PRIMARY KEY will be set by application");
			}
			Beer result = beerService.save(beer);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}

	}

	/**
	 * {@code PUT  /beers} : Updates an existing beer.
	 *
	 * @param beer the beer to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated beer, or with status {@code 417 (Expectation Failed)} if
	 *         the beer is not valid.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/beers")
	public ResponseEntity<Beer> updateBeer(@Valid @RequestBody Beer beer) throws URISyntaxException {
		log.debug("REST request to update Beer : {}", beer);

		try {
			if (beer.getId() == null) {
				throw new ApiException("A beer must have an ID to update",
						"PRIMARY KEY is required to udpate an entity");
			}
			Beer result = beerService.save(beer);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}

	}
	
	/**
	 * {@code GET  /beers} : get all the beers.
	 *
	 * @param pageable the pagination information.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of beers in body.
	 */
	@GetMapping("/beers")
	public ResponseEntity<List<Beer>> getAllBeers() {
		log.debug("REST request to get a page of beers");
		List<Beer> page = beerService.findAll();
		return ResponseEntity.ok().body(page);
	}


	/**
	 * {@code GET  /beers/:id} : get the "id" beer.
	 *
	 * @param id the id of the beer to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the beer, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/beers/{id}")
	public ResponseEntity<Beer> getBeer(@PathVariable Long id) {
		log.debug("REST request to get Beer : {}", id);
		Optional<Beer> beer = beerService.findOne(id);
		if (beer.isPresent()) {
			return new ResponseEntity<>(beer.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * {@code DELETE  /beer/:id} : delete the "id" beer.
	 *
	 * @param id the id of the beer to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}, or
	 *         with status {@code 417 (Expectation Failed)} if the operation failed.
	 */
	@DeleteMapping("/beers/{id}")
	public ResponseEntity<Void> deleteBeer(@PathVariable Long id) {
		log.debug("REST request to delete Beer : {}", id);

		try {
			beerService.delete(id);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.EXPECTATION_FAILED);
		}
	}

}
