package com.example.beerme.graphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main boot application
 * 
 * 
 *
 */
@SpringBootApplication
public class RestGraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestGraphqlApplication.class, args);
	}

}
