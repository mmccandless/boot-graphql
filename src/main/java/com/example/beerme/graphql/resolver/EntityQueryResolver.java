package com.example.beerme.graphql.resolver;

import java.util.List;
import java.util.Optional;

import com.example.beerme.graphql.domain.Beer;
import com.example.beerme.graphql.domain.Brewer;
import com.example.beerme.graphql.service.BeerService;
import com.example.beerme.graphql.service.BrewerService;

import org.springframework.stereotype.Component;

import graphql.kickstart.tools.GraphQLQueryResolver;

/**
 * GraphQL query resolvers that delegates service methods of all the entities
 * 
 * @see BrewerService
 * @see BeerService
 * 
 * 
 *
 */
@Component
public class EntityQueryResolver implements GraphQLQueryResolver {

	private final BrewerService brewerService;
	private final BeerService beerService;

	/**
	 * Constructor that takes instances of injected service bean for all entities
	 * 
	 * @param brewerService An instance of Spring managed BrewerService bean
	 * @param beerService An instance of Spring managed BeerService bean
	 */
	public EntityQueryResolver(BrewerService brewerService,
		BeerService beerService) {
		this.brewerService = brewerService;
		this.beerService = beerService;
	}

	/**
	 * Returns an Brewer for the given id
	 * 
	 * @param id Primary key of the Brewer
	 * @return An instance of Brewer
	 */
	public Brewer getBrewerById(Long id) {
		try {
			Optional<Brewer> brewer = brewerService.findOne(id);
			if (brewer.isPresent()) {
				brewer.get().getbeers();
				return brewer.get();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * Returns a Beer for the given id
	 * 
	 * @param id Primary key of the Beer
	 * @return An instance of Beer
	 */
	public Beer getBeerById(Long id) {
		try {
			Optional<Beer> beer = beerService.findOne(id);
			if (beer.isPresent()) {
				return beer.get();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	public List<Beer> getAllBeers() {
		try {
			List<Beer> beers = beerService.findAll();
			if (!beers.isEmpty()) {
				return beers;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
}
