package com.example.beerme.graphql.resolver;

import com.example.beerme.graphql.domain.Beer;
import com.example.beerme.graphql.domain.Brewer;
import com.example.beerme.graphql.service.BeerService;
import com.example.beerme.graphql.service.BrewerService;

import org.springframework.stereotype.Component;

import graphql.kickstart.tools.GraphQLMutationResolver;

/**
 * GraphQL mutation resolver that delegates service methods of all the entities
 * 
 * @see BrewerService
 * @see beerservice
 *
 */
@Component
public class EntityMutationResolver implements GraphQLMutationResolver {

	private final BrewerService brewerService;
	private final BeerService beerService;

	/**
	 * Constructor that takes instances of injected service bean for all entities
	 * 
	 * @param brewerService An instance of Spring managed BrewerService bean
	 * @param beerService An instance of Spring managed BeerService bean
	 */
	public EntityMutationResolver( BrewerService brewerService,
			BeerService beerService) {
		this.brewerService = brewerService;
		this.beerService = beerService;
	}

	/**
	 * Adds a Brewer entity
	 * 
	 * @param name Name of the Brewer
	 * @return An instance of Brewer if created successfully
	 * @throws Exception If any occurred
	 */
	public Brewer addBrewer(String name) throws Exception {
		Brewer brewer = new Brewer(name);
		return brewerService.save(brewer);
	}

	/**
	 * Adds a Beer entity
	 * 
	 * @param title Title of the Beer
	 * @param brewerId Id of the parent Brewer
	 * @return An instance of Beer if created successfully
	 * @throws Exception If any occurred
	 */
	public Beer addBeer(String title, Long brewerId) throws Exception {
		Beer beer = new Beer(title);
		beer.setBrewer(brewerService.findOne(brewerId).get());
		return beerService.save(beer);
	}

}