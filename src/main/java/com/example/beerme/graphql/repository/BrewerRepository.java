package com.example.beerme.graphql.repository;

import com.example.beerme.graphql.domain.Brewer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the Brewer entity
 * 
 * 
 *
 */
@SuppressWarnings("unused")
@Repository
public interface BrewerRepository extends JpaRepository<Brewer, Long> {
}
