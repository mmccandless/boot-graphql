package com.example.beerme.graphql.repository;

import com.example.beerme.graphql.domain.Beer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the Beer entity
 * 
 * 
 *
 */
@SuppressWarnings("unused")
@Repository
public interface BeerRepository extends JpaRepository<Beer, Long> {
}
